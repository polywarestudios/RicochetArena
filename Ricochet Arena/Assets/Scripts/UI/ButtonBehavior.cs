﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonBehavior : EventTrigger, ISelectHandler
{
  ButtonHoverSound buttonHoverSound;

	// Use this for initialization
	void Start () {
		buttonHoverSound = gameObject.GetComponentInChildren(typeof(ButtonHoverSound)) as ButtonHoverSound;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public override void OnPointerEnter(PointerEventData eventData)
  {
    GetComponent<RectTransform>().localScale = new Vector3(1.1f, 1.1f, 1.0f);
    buttonHoverSound.Play();
  }
  public override void OnPointerExit(PointerEventData eventData)
  {
    GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
  }

  public override void OnSelect(BaseEventData eventData)
  {
    GetComponent<RectTransform>().localScale = new Vector3(1.1f, 1.1f, 1.0f);
    buttonHoverSound.Play();
  }
  public override void OnDeselect(BaseEventData eventData)
  {
    GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
  }

}
