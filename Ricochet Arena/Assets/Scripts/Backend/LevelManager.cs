﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  // Image, Color
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
  
  Image fade;
  float alphaMin = 0;
  float alphaMax = 1;
  
  float timer = 0;
  
	// Use this for initialization
	void Start () {
		fade = GameObject.Find("Fade").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void LoadScene(string levelName)
  {
    timer = 0;
    StartCoroutine(FadeOut(1.0f, levelName));
  }

  public void ExitApplication()
  {
    Application.Quit();
  }
  
  IEnumerator FadeOut(float time, string levelName)
  {
    while (fade.color.a < alphaMax)
    {
      timer += Time.deltaTime;
      float ratio = timer / time;
      
      Color tempColor = fade.color;
      tempColor.a = Mathf.Lerp(alphaMin, alphaMax, ratio);
      fade.color = tempColor;
      
      yield return null;
    }
    
    SceneManager.LoadScene(levelName);
  }
}
