﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicObjects : MonoBehaviour {

  public static GameObject AddPrefabToDynamicFolder(GameObject prefab)
  {
    // Find the Dynamic Objects folder
    GameObject __folder = GameObject.Find("DynamicObjects");

    // If the Dynamic Objects folder doesn't exist, create it
    if (__folder == null)
    {
      __folder = Instantiate(Resources.Load("Prefabs/Utilities/DynamicObjects") as GameObject);
      __folder.name = "DynamicObjects";
    }

    // Parent the prefab to the folder
    prefab.transform.parent = __folder.transform;

    return prefab;
  }

  // Use this for initialization
  void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
