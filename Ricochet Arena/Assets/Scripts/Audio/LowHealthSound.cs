﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowHealthSound : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;

  AudioSource source;
  AudioManager audioManager;

	// Use this for initialization
	void Start () {
		source = gameObject.GetComponent<AudioSource>();
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
  
  public void Play()
  {
    audioManager.Loop(source, clip, dB, semitones);
  }
  
  public void Stop()
  {
    source.Stop();
  }

}
