﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeSound : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;

  AudioSource source;
  AudioManager audioManager;

	// Use this for initialization
	void Start () {
		source = gameObject.GetComponent<AudioSource>();
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
  
  public void Play()
  {
    audioManager.Play(source, clip, dB, semitones);
  }

}
