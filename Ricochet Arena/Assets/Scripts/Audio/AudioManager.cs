﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
  
  // Use this for initialization
  void Start () {
    
  }

  // Update is called once per frame  
  void Update () {
  
  }
  
  // Converts dB to Unity volume
  public float dBToVol(float dB)
  {
    float vol = Mathf.Pow(10f, (dB / 20f));
    
    return vol;
  }
  
  // Converts semitones to Unity pitch
  float SemitonesToPitch(float semitones)
  {
      float unityPitch = Mathf.Pow(2, (semitones / 12f));
      
      return unityPitch;
  }
  
  void PlayClip(AudioSource s, AudioClip ac, float dB, float semitones)
  {
    s.clip = ac;
    s.volume = dBToVol(dB);
    s.pitch = SemitonesToPitch(semitones);
    s.Play();
  }
  
  public void Play(AudioSource s, AudioClip ac, float dB, float semitones)
  {
    s.loop = false;
    PlayClip(s, ac, dB, semitones);
  }
  
  public void Loop(AudioSource s, AudioClip ac, float dB, float semitones)
  {
    s.loop = true;
    PlayClip(s, ac, dB, semitones);
  }
}
