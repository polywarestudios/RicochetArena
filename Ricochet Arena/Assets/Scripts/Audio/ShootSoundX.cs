﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSoundX : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;

  AudioSource[] source;
  AudioManager audioManager;
  int currentSource = 1;
  int numOfSources = 16;

	// Use this for initialization
	void Start () {
    // Find all the audio sources used for shoot sounds
    Transform parent = transform.parent;
    source = new AudioSource[numOfSources];
    
		for (int x = 1; x <= numOfSources; x++)
    {
      string sourceN = string.Concat("ShootSource", x.ToString());
      source[x - 1] = parent.Find(sourceN).GetComponent<AudioSource>();
    }
    
    // Find the audio manager
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
  
  public void Play()
  {
    // Cycle between playing it on 16 different sources, to have 16 simultaneous audio channels
    audioManager.Play(source[currentSource - 1], clip, dB, semitones);
    if ((++currentSource) > numOfSources)
      currentSource = 1;
  }

}
