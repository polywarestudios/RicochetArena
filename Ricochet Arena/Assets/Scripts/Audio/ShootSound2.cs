﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSound2 : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;
  public float rate;
  private float timer = 0.0f;

  AudioSource source;
  AudioManager audioManager;

	// Use this for initialization
	void Start () {
		source = gameObject.GetComponent<AudioSource>();
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
  
  public void Play()
  {
    if (timer < 0)
    {
      timer = rate;
      audioManager.Play(source, clip, dB, semitones);
    }
    else
    {
      timer -= Time.deltaTime;
    }
  }

}
