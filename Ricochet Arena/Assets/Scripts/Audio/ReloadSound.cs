﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSound : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;
  private bool playOnce = false;
  
  AudioSource source;
  AudioManager audioManager;

	// Use this for initialization
	void Start () {
    source = gameObject.GetComponent<AudioSource>();
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
  
  public void Play()
  {
    if (playOnce == false)
    {
      playOnce = true;
      audioManager.Play(source, clip, dB, semitones);
    }
  }
  
  public void Stop()
  {
    playOnce = false;
    source.Stop();
  }
}
