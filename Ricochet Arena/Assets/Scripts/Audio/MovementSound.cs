﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSound : MonoBehaviour {

  public AudioClip clip;
  public float dB;
  public float semitones;

  AudioSource source;
  AudioManager audioManager;
  
  float timer = 0;
  float fadeTime = 1;
  float currentVol = 0;
  bool fadingIn = false;
  bool fadingOut = false;

	// Use this for initialization
	void Start () {
		source = gameObject.GetComponent<AudioSource>();
    audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    
    Play();
	}
	
	// Update is called once per frame
	void Update () {
		if (fadingIn == true)
    {
      // Fade in over time
      timer += Time.deltaTime;
      
      // Interpolate between no volume and desired volume
      float ratio = (timer / fadeTime);
      source.volume = audioManager.dBToVol( Mathf.Lerp(-96.0f, dB, ratio) );
      
      // End fade in if it reaches or passes desired volume
      if (ratio >= 1)
      {
        source.volume = audioManager.dBToVol(dB);
        fadingIn = false;
      }
    }
    
    else if (fadingOut == true)
    {
      // Fade out over time
      timer -= Time.deltaTime;
      
      // Interpolate between current volume and no volume
      float ratio = (timer / fadeTime);
      source.volume = (ratio * currentVol);
      
      // End fade out if it reaches or passes no volume
      if (ratio <= 0)
      {
        source.volume = 0;
        fadingIn = false;
      }
    }
	}
  
  public void Play()
  {
    audioManager.Loop(source, clip, -96.0f, semitones);
  }
  
  public void Stop()
  {
    source.Stop();
  }
  
  public void FadeIn(float t)
  {
    // Just play if fade time is 0
    if (t <= 0)
    {
      audioManager.Loop(source, clip, dB, semitones);
      return;
    }
    
    audioManager.Loop(source, clip, -96.0f, semitones);
    fadeTime = t;
    timer = 0;
    fadingIn = true;
    fadingOut = false;
  }

  public void FadeOut(float t)
  {
    // Just stop if fade time is 0 or sound is already off
    if (t <= 0 || source.volume <= 0)
    {
      source.Stop();
      return;
    }
    
    fadeTime = t;
    timer = t;
    currentVol = source.volume;
    fadingIn = false;
    fadingOut = true;
  }
  
  public void ScaleVol(float scalar)
  {
    source.volume = scalar * audioManager.dBToVol(dB);
  }
}
