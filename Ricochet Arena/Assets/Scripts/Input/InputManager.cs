﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

  #region Player Movement
  public delegate void OnPlayerMove(Vector2 input, int playerNo);
  public static event OnPlayerMove PlayerMove;

  public delegate void OnPlayerAim(Vector2 input, int playerNo);
  public static event OnPlayerAim PlayerAim;

  public delegate void OnPlayerShoot(float value, int playerNo);
  public static event OnPlayerShoot PlayerShoot;
  #endregion

  // Use this for initialization
  void Start ()
  {
    
  }

  // Update is called once per frame
  void Update () 
  {
    #region Move Events

    //Debug.Log(new Vector2(Input.GetAxisRaw("P1_Horiz"), Input.GetAxisRaw("P1_Vert")));
    PlayerMove(new Vector2(Input.GetAxisRaw("P1_Horiz"), Input.GetAxisRaw("P1_Vert")), 1);
    PlayerMove(new Vector2(Input.GetAxisRaw("P2_Horiz"), Input.GetAxisRaw("P2_Vert")), 2);
    PlayerMove(new Vector2(Input.GetAxisRaw("P3_Horiz"), Input.GetAxisRaw("P3_Vert")), 3);
    PlayerMove(new Vector2(Input.GetAxisRaw("P4_Horiz"), Input.GetAxisRaw("P4_Vert")), 4);

    PlayerAim(new Vector2(Input.GetAxisRaw("P1_Aim_Horiz"), Input.GetAxisRaw("P1_Aim_Vert")), 1);
    PlayerAim(new Vector2(Input.GetAxisRaw("P2_Aim_Horiz"), Input.GetAxisRaw("P2_Aim_Vert")), 2);
    PlayerAim(new Vector2(Input.GetAxisRaw("P3_Aim_Horiz"), Input.GetAxisRaw("P3_Aim_Vert")), 3);
    PlayerAim(new Vector2(Input.GetAxisRaw("P4_Aim_Horiz"), Input.GetAxisRaw("P4_Aim_Vert")), 4);

    #endregion

    PlayerShoot(Input.GetAxisRaw("P1_Shoot"), 1);
    PlayerShoot(Input.GetAxisRaw("P2_Shoot"), 2);
    PlayerShoot(Input.GetAxisRaw("P3_Shoot"), 3);
    PlayerShoot(Input.GetAxisRaw("P4_Shoot"), 4);
  }
}
