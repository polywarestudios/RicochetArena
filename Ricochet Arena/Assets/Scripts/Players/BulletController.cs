﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(Material))]
public class BulletController : MonoBehaviour {

  float speed = 150.0f;
  public int playerNo;

  int maxBounceCount = 10;
  int bounceCount;
  Rigidbody thisRigidbody;

  // Use this for initialization
  void Start () {
    thisRigidbody = GetComponent<Rigidbody>();
    thisRigidbody.velocity = (transform.forward * speed);

    // Set the parent to clean up the editor
    gameObject.transform.SetParent(GameObject.FindGameObjectWithTag("BulletList").GetComponent<Transform>());
  }	
  // Update is called once per frame
  void Update () {
    
  }

  private void OnCollisionEnter(Collision collision)
  {
    if(collision.gameObject.tag == "BounceWall")
    {
      bounceCount++;

      if (bounceCount == maxBounceCount)
      {
        Destroy(gameObject);
        return;
      }

    }

    else
    {
        Destroy(gameObject);
    }
  }
}
