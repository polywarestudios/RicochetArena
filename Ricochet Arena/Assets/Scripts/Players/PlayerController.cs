﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


  public int playerNo;
  public GameObject respawnPoint;

  Vector3 playerDirection;
  float playerSpeed = 30.0f;
  int maxHealth = 20;
  int currHealth;
  int alertHealth = 5;
  
  DamageSound damageSound;
  DeathSound deathSound;
  MovementSound movementSound;
  LowHealthSound lowHealthSound;
  LowHealthSound2 lowHealthSound2;
  bool lowHealthSoundPlaying = false;

 private void OnEnable()
  {
    // Subscribe to Player Events
    InputManager.PlayerMove += MovePlayer;
    InputManager.PlayerAim += AimPlayer;
  }

  // The number is the player number
  void MovePlayer(Vector2 input, int number)
  {

    // Only move this player with the correct input
    if(number == playerNo)
    {
      // Move the player based on the right stick
      transform.position += (playerSpeed * new Vector3(input.x, 0f, -input.y)) * Time.deltaTime;

      // Make the player face the direction they are moving
      playerDirection = new Vector3(input.x, 0f, -input.y);
      if (playerDirection != Vector3.zero)
      {
        transform.rotation = Quaternion.LookRotation(playerDirection);
      }  
        
      // Movement SFX vol based on speed
      float scalar = Mathf.Sqrt(Mathf.Abs(input.x) * Mathf.Abs(input.x) + Mathf.Abs(input.y) * Mathf.Abs(input.y));
      movementSound.ScaleVol(scalar);
    }
  }

  // The number is the player number
  void AimPlayer(Vector2 input, int number)
  {
    
    // Only move this player with the correct input
    if (number == playerNo)
    {
      // Make the player face where the right stick is
      playerDirection = new Vector3(input.x, 0f, -input.y);
      if(playerDirection != Vector3.zero)
      {
        transform.rotation = Quaternion.LookRotation(playerDirection);
      }
    }
  }

  // Use this for initialization
  void Start () {
    currHealth = maxHealth;
    
    damageSound = gameObject.GetComponentInChildren(typeof(DamageSound)) as DamageSound;
    deathSound = gameObject.GetComponentInChildren(typeof(DeathSound)) as DeathSound;
    movementSound = gameObject.GetComponentInChildren(typeof(MovementSound)) as MovementSound;
    lowHealthSound = gameObject.GetComponentInChildren(typeof(LowHealthSound)) as LowHealthSound;
    lowHealthSound2 = gameObject.GetComponentInChildren(typeof(LowHealthSound2)) as LowHealthSound2;
  }
	
  // Update is called once per frame
  void Update () {
    if(Input.GetKeyDown(KeyCode.K)){
      Die();
    }
  }

  public void Die()
  {
    Debug.Log("Player " + playerNo + " died!");
    transform.position = new Vector3(respawnPoint.transform.position.x, transform.position.y, respawnPoint.transform.position.z);
    deathSound.Play();
    
  } 

  void OnCollisionEnter(Collision collision)
  {
    // If a bullet collides with the player
    if(collision.gameObject.tag == "Bullet")
    {
      damageSound.Play();
      
      // If after we decrement we are at 0
      if(--currHealth == 0)
      {
        Die();
        lowHealthSound.Stop();
      }
      
      // Looping low health SFX
      else if (currHealth <= (alertHealth) && lowHealthSoundPlaying == false)
      {
        lowHealthSoundPlaying = true;
        lowHealthSound.Play();
        lowHealthSound2.Play();
      }
    }
  }
}
