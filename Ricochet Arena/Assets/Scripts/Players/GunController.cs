﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

  public BulletController bullet;
  public float offset;
  public float reloadTimerMax;
  public int maxAmmo;
  public float rateOfFire;

  int currentBullets;
  float reloadTimer;
  PlayerController player;
  float shotDeadZone = 0.5f;
  bool isReloading = false;
  bool isFiring = false;
  
  ShootSoundX shootSoundX;
  ReloadSound reloadSound;
  ReloadSound2 reloadSound2;
  ReloadSound3 reloadSound3;

  private void OnEnable()
  {
    // Subscribe to Player Events
    InputManager.PlayerShoot += Fire;
  }

  // Use this for initialization
  void Start () {
    player = GetComponentInParent<PlayerController>();
    currentBullets = maxAmmo;
    reloadTimer = reloadTimerMax;
    
    Transform parent = transform.parent;
    shootSoundX = parent.Find("ShootSound/ShootSoundX").GetComponent<ShootSoundX>();
    reloadSound = parent.Find("ReloadSound").GetComponent<ReloadSound>();
    reloadSound2 = parent.Find("ReloadSound2").GetComponent<ReloadSound2>();
    reloadSound3 = parent.Find("ReloadSound3").GetComponent<ReloadSound3>();
  }
	
  // Update is called once per frame
  void Update () {
    if (isReloading == true)
    {
      reloadSound.Play();
      reloadSound2.Play();
      Reload();
    }
  }

  void Reload()
  {
    if(reloadTimer < 0)
    {
      isReloading = false;
      currentBullets = maxAmmo;
      reloadTimer = reloadTimerMax;
      reloadSound.Stop();
      reloadSound2.Stop();
      reloadSound3.Play();
    }
    else
    {
      reloadTimer -= Time.deltaTime;
    }
  }

  IEnumerator WaitToFire()
  {
    isFiring = true;
    yield return new WaitForSeconds(rateOfFire);
    isFiring = false;
  }

  void Fire(float input, int num)
  {
    if(((input > shotDeadZone) && (num == player.playerNo)))
    {
      // If there is enough bullets
      if (currentBullets > 0)
      {
        if (isFiring == false)
        {
          currentBullets--;
          BulletController newBullet = Instantiate(bullet, transform.position, transform.rotation) as BulletController;
          newBullet.GetComponent<Transform>().rotation = Quaternion.LookRotation(player.transform.forward);
          shootSoundX.Play();
          
          StartCoroutine(WaitToFire());
        }
      }
      else
      {
        isReloading = true;
      }
    }
  }
}
